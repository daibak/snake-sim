use puzzle::elements::{Cell, Direction};

use crate::agent::*;

// A zig-zag basic agent
// only works with arena where:
// assert!(arena_width % 2 == 0);
pub struct FixedZigZag {
    arena_width: u8,
    arena_height: u8,
}
impl FixedZigZag {
    pub(crate) fn new(arena_width: u8, arena_height: u8) -> Self {
        Self {
            arena_width,
            arena_height,
        }
    }
}

impl Agent for FixedZigZag {
    // In the bottom row go right.
    // In the middle go up and down moving left
    // In the top row go left or down
    fn act(&mut self, obs: Obs) -> Direction {
        let &Cell { x, y } = obs.0.head();

        if y == 0 {
            if x == self.arena_width - 1 {
                return Direction::Up;
            } else {
                return Direction::Right;
            }
        }

        if y == self.arena_height - 1 {
            if x % 2 == 1 {
                return Direction::Left;
            } else {
                return Direction::Down;
            }
        }

        if y == 1 {
            if x == 0 {
                return Direction::Down;
            }
            if x == self.arena_width {
                return Direction::Up;
            }
            if x % 2 == 0 {
                return Direction::Left;
            } else {
                return Direction::Up;
            }
        }

        if x % 2 == 0 {
            Direction::Down
        } else {
            Direction::Up
        }
    }
}
