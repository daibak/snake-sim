use puzzle::{
    elements::Direction,
    game::{self, Event, State},
};

use serde::{Deserialize, Serialize};

use crate::zigzag::FixedZigZag;

pub struct Obs<'a>(pub &'a State, pub Event);

pub trait Agent {
    fn act(&mut self, state: Obs) -> Direction;
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum Config {
    Constant(Direction),
    FixedZigZag(),
}

impl Config {
    pub fn build(self, game_config: &game::Config) -> Box<dyn Agent> {
        match self {
            Config::Constant(dir) => Box::new(Constant::new(dir)),
            Config::FixedZigZag() => {
                let (arena_width, arena_height) = game_config.arena_size();
                Box::new(FixedZigZag::new(arena_width, arena_height))
            }
        }
    }
}

#[derive(Debug, Clone, Default)]
pub struct Constant(Direction);

impl Agent for Constant {
    fn act(&mut self, _: Obs) -> Direction {
        self.0
    }
}
impl Constant {
    fn new(dir: Direction) -> Self {
        Self(dir)
    }
}
