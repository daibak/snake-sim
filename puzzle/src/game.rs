use rand::{rngs::StdRng, Rng, SeedableRng};
use serde::{Deserialize, Serialize};
use std::{
    collections::{HashSet, VecDeque},
    fmt::{self, Display, Write},
};
use tracing::{info, instrument, trace};

use crate::elements::*;

pub const DEFAULT_ARENA_WIDTH: u8 = 10;
pub const DEFAULT_ARENA_HEIGHT: u8 = 10;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Config {
    arena_width: u8,
    arena_height: u8,
    starting_cell: Cell,
    starting_dir: Direction,
    seed: u64,
}

impl Default for Config {
    fn default() -> Self {
        Config {
            arena_width: DEFAULT_ARENA_WIDTH,
            arena_height: DEFAULT_ARENA_HEIGHT,
            starting_cell: Cell {
                x: DEFAULT_ARENA_WIDTH / 2,
                y: DEFAULT_ARENA_HEIGHT / 2,
            },
            starting_dir: Direction::default(),
            seed: 0,
        }
    }
}

impl Config {
    pub fn new(
        arena_width: u8,
        arena_height: u8,
        starting_cell: Cell,
        starting_dir: Direction,
        seed: u64,
    ) -> Self {
        Self {
            arena_width,
            arena_height,
            starting_cell,
            starting_dir,
            seed,
        }
    }
    pub fn new_random_with_seed(seed: u64) -> Self {
        let mut rng = StdRng::seed_from_u64(seed);
        let arena_width = rng.gen_range(2..u8::MAX);
        let arena_height = rng.gen_range(2..u8::MAX);
        Config {
            arena_width,
            arena_height,
            starting_cell: Cell {
                x: rng.gen_range(0..arena_width),
                y: rng.gen_range(0..arena_height),
            },
            starting_dir: match rng.gen_range(0..=3) {
                0 => Direction::Down,
                1 => Direction::Left,
                2 => Direction::Right,
                3 => Direction::Up,
                _ => unreachable!(),
            },
            seed: rng.gen(),
        }
    }
    pub fn new_random() -> Self {
        Self::new_random_with_seed(rand::random())
    }

    pub fn arena_size(&self) -> (u8, u8) {
        (self.arena_width, self.arena_height)
    }

    pub fn add_to_seed(&mut self, seed: u64) {
        self.seed += seed;
    }

}

#[derive(Debug, Deserialize, Serialize)]
pub struct State {
    snake: VecDeque<Cell>,
    head_dir: Direction,
    food: Option<Cell>,
    steps: usize,
    status: Status,
    config: Config,
    #[serde(skip, default = "default_rng")]
    rng: rand::rngs::StdRng,
}

fn default_rng() -> StdRng {
    StdRng::seed_from_u64(0)
}

impl Default for State {
    fn default() -> Self {
        let config = Config::default();
        State::from_config(config)
    }
}

impl Display for State {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let snake: HashSet<(u8, u8)> = self.snake.iter().map(|c| (c.x, c.y)).collect();
        let head = self.head();
        let head_dir = self.head_dir;
        let border = std::iter::repeat('-')
            .take(self.config.arena_width as usize)
            .chain("\n".chars())
            .collect::<String>();
        f.write_str(border.as_str())?;
        for y in (0..self.config.arena_height).rev() {
            for x in 0..self.config.arena_width {
                let mut curr = ' ';
                if snake.contains(&(x, y)) {
                    curr = '#';
                }
                if head.eq(&Cell { x, y }) {
                    curr = match head_dir {
                        Direction::Left => '🡠',
                        Direction::Down => '🡣',
                        Direction::Right => '🡢',
                        Direction::Up => '🡡',
                    };
                }
                if self.food.eq(&Some(Cell { x, y })) {
                    curr = '?';
                }
                f.write_char(curr)?;
            }
            f.write_char('\n')?;
        }
        f.write_str(border.as_str())?;
        Ok(())
    }
}

impl State {
    pub fn from_config(config: Config) -> Self {
        State {
            snake: VecDeque::from([config.starting_cell.clone()]),
            head_dir: config.starting_dir,
            food: None,
            steps: 0,
            status: Status::Init,
            rng: StdRng::seed_from_u64(config.seed),
            config,
        }
    }

    pub fn head(&self) -> &Cell {
        self.snake.front().expect("queue should never be empty")
    }
    pub fn head_dir(&self) -> Direction {
        self.head_dir
    }
    pub fn snake(&self) -> &VecDeque<Cell> {
        &self.snake
    }

    fn spawn_food(&mut self) {
        let snake: HashSet<(u8, u8)> = self.snake.iter().map(|c| (c.x, c.y)).collect();
        let snake_length = snake.len();
        let arena_size = self.arena_size();
        let free = arena_size - snake_length;
        if free == 0 {
            return;
        }
        // optimization depending on snake length
        // if snake short, random cell and check not on snake
        // if long pick random cell from free ones (need to allocate full grid though)
        let mut food;
        if snake_length < arena_size / 2 {
            loop {
                food = Cell {
                    x: self.rng.gen_range(0..self.config.arena_width),
                    y: self.rng.gen_range(0..self.config.arena_height),
                };
                if !snake.contains(&(&food).into()) {
                    break;
                }
            }
        } else {
            let n = self.rng.gen_range(0..free);
            let (x, y) = (0..self.config.arena_width)
                .flat_map(|y| (0..self.config.arena_height).map(move |x| (x, y)))
                .filter(|t| !snake.contains(t))
                .nth(n)
                .expect("should exist as there are {free} free elements");
            // alternatively make a difference betwenn hashsets but needs to allocate full grid
            // let (x, y) = *grid.difference(&snake)
            food = Cell { x, y };
        }

        info!("spawn food at {food:?}");
        self.food = Some(food);
    }

    #[instrument(level = "trace", skip(self), ret, fields(snake = ?self.snake, head_dir = ?self.head_dir, steps = self.steps ))]
    pub fn step(&mut self, dir: Direction) -> Event {
        match self.status {
            Status::Init => {
                self.status = Status::Running;
                if self.food.is_none() {
                    self.spawn_food();
                }
            }
            Status::Running => (),
            Status::Ended => return Event::AlreadyEnded,
        }
        self.steps += 1;

        if dir != !self.head_dir {
            self.head_dir = dir;
            trace!("change dir {dir:?}");
        }

        let old_head = self.head();
        let head = old_head.checked_move(self.head_dir);
        trace!("new head at {head:?}");

        let head = match head {
            Some(head) if !self.done(&head) => {
                self.snake.push_front(head.clone());
                head
            }
            Some(_) | None => return self.end_game(),
        };

        match &self.food {
            Some(food) if head == *food => {
                self.food = None;
                info!("eat, grow (no shrink)");
                if self.snake.len() == self.arena_size() {
                    return self.end_game();
                }
                self.spawn_food();
                Event::Eat
            }
            Some(_) | None => {
                trace!("move only (no growth)");
                self.snake.pop_back();
                Event::Move
            }
        }
    }

    fn done(&self, cell: &Cell) -> bool {
        cell.x >= self.config.arena_width
            || cell.y >= self.config.arena_height
            || self.snake.iter().skip(1).any(|i| i == cell)
            || self.snake.len() == self.arena_size()
    }

    fn arena_size(&self) -> usize {
        self.config.arena_height as usize * self.config.arena_width as usize
    }

    fn end_game(&mut self) -> Event {
        info!("Done! Game end");
        self.status = Status::Ended;
        Event::Done
    }

    pub fn is_done(&self) -> bool {
        matches!(self.status, Status::Ended)
    }

    pub fn results(&self) -> Results {
        let snake_length = self.snake.len();
        Results {
            steps: self.steps,
            win: snake_length == self.arena_size(),
            snake_length: snake_length - 1,
        }
    }
}

#[derive(Default, Debug, Serialize, Deserialize)]
pub enum Status {
    #[default]
    Init,
    Running,
    Ended,
}

#[derive(Debug, PartialEq)]
pub enum Event {
    Init,
    Move,
    Eat,
    Done,
    AlreadyEnded,
}

#[derive(Default, Debug)]
pub struct Results {
    win: bool,
    steps: usize,
    snake_length: usize,
}

impl fmt::Display for Results {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "(win={}, steps={}, food_eaten={})",
            self.win, self.steps, self.snake_length
        )
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn basic_steps() {
        let mut game_state = State::default();

        game_state.step(Direction::Down);
        game_state.step(Direction::Up);
        game_state.step(Direction::Left);

        assert_eq!(game_state.head_dir, Direction::Left);
        assert_eq!(game_state.steps, 3);
        assert_eq!(
            game_state.snake,
            VecDeque::from([Cell {
                x: DEFAULT_ARENA_WIDTH / 2 - 1,
                y: DEFAULT_ARENA_HEIGHT / 2 + 2
            }])
        );
    }
}
