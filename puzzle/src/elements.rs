use serde::{Deserialize, Serialize};
use std::ops::Not;

#[derive(Debug, Default, Copy, Clone, PartialEq, Deserialize, Serialize)]
pub enum Direction {
    Left,
    Down,
    Right,
    #[default]
    Up,
}
impl Not for Direction {
    type Output = Direction;
    fn not(self) -> Direction {
        match self {
            Direction::Left => Direction::Right,
            Direction::Down => Direction::Up,
            Direction::Right => Direction::Left,
            Direction::Up => Direction::Down,
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq, Deserialize, Serialize)]
pub struct Cell {
    pub x: u8,
    pub y: u8,
}

impl Cell {
    pub fn new(x: u8, y: u8) -> Self {
        Self { x, y }
    }
    pub fn checked_move(&self, dir: Direction) -> Option<Cell> {
        match dir {
            Direction::Left => {
                let x = self.x.checked_sub(1)?;
                Some(Cell { x, ..*self })
            }
            Direction::Down => {
                let y = self.y.checked_sub(1)?;
                Some(Cell { y, ..*self })
            }
            Direction::Right => {
                let x = self.x.checked_add(1)?;
                Some(Cell { x, ..*self })
            }
            Direction::Up => {
                let y = self.y.checked_add(1)?;
                Some(Cell { y, ..*self })
            }
        }
    }
    pub fn manhattan_distance(&self, other: &Cell) -> u8 {
        self.x.abs_diff(other.x) + self.y.abs_diff(other.y)
    }
    pub fn neighbour(&self, other: &Cell) -> bool {
        self.manhattan_distance(other) == 1
    }
}

impl From<&Cell> for (u8, u8){
    fn from(value: &Cell) -> Self {
        (value.x, value.y)
    }
}