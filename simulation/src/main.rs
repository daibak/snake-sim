use anyhow::Result;

mod cli;
mod config;
mod runner;
mod tracing;

fn main() -> Result<()> {
    let cli = cli::parse_cli();
    let config = config::load(&cli)?;
    println!("{config:#?}");

    tracing::setup(&config)?;

    match &cli.command {
        cli::Command::Run => {
            let game_results = runner::play_agent(config.game_config, config.agent_config);
            println!("{game_results}");
        }
        cli::Command::BatchRun(_) => {
            let game_stats = runner::batch_play_agent(
                config.game_config,
                config.agent_config,
                config.batch_config.expect("batch-run command implies the relevant config has been merged, eventually the default cli one"),
            );
            println!("{game_stats:#?}");
        }
        cli::Command::PrintConfig => {
            std::fs::write("config_all.yaml", serde_yaml::to_string(&config).unwrap())
                .expect("local dir should be writeable to save in config.yaml");
        }
    }
    Ok(())
}
