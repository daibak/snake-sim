use std::{fs, path::PathBuf};

use anyhow::{Context, Result};
use figment::{
    providers::{Format, Serialized, Yaml},
    Figment,
};
use serde::{Deserialize, Serialize};
use tracing_core::LevelFilter;

use crate::cli::{self, Cli};
use agents::agent;
use puzzle::game;

#[derive(Debug, Default, Serialize, Deserialize)]
pub struct BatchConfig {
    pub n_runs: usize,
    pub threads: usize,
}

impl From<&cli::BatchConfig> for BatchConfig {
    fn from(v: &cli::BatchConfig) -> BatchConfig {
        BatchConfig {
            n_runs: v.n_runs,
            threads: v.threads,
        }
    }
}

#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub enum TraceLevel {
    Off,
    Info,
    Debug,
    Trace,
}

impl From<&cli::TraceLevel> for TraceLevel {
    fn from(v: &cli::TraceLevel) -> TraceLevel {
        match v {
            cli::TraceLevel::Off => TraceLevel::Off,
            cli::TraceLevel::Info => TraceLevel::Info,
            cli::TraceLevel::Debug => TraceLevel::Debug,
            cli::TraceLevel::Trace => TraceLevel::Trace,
        }
    }
}

impl From<TraceLevel> for LevelFilter {
    fn from(v: TraceLevel) -> LevelFilter {
        match v {
            TraceLevel::Off => LevelFilter::OFF,
            TraceLevel::Info => LevelFilter::INFO,
            TraceLevel::Debug => LevelFilter::DEBUG,
            TraceLevel::Trace => LevelFilter::TRACE,
        }
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Config {
    pub verbosity: TraceLevel,
    pub log_file: Option<PathBuf>,
    pub game_config: game::Config,
    pub agent_config: agent::Config,
    pub batch_config: Option<BatchConfig>,
}

// The default configuration.
impl Default for Config {
    fn default() -> Self {
        Config {
            verbosity: TraceLevel::Info,
            log_file: None,
            game_config: game::Config::new_random(),
            agent_config: agent::Config::FixedZigZag(),
            // agent_config: agent::Config::Constant(puzzle::elements::Direction::default()),
            batch_config: None,
        }
    }
}

fn des_yaml_file<T: for<'a> Deserialize<'a>>(path: &PathBuf, name: &str) -> Result<T> {
    serde_yaml::from_str(
        fs::read_to_string(path)
            .context(format!("invalid path for {name}"))?
            .as_str(),
    )
    .context(format!("invalid {name} in yaml"))
}

pub fn load(cli: &Cli) -> Result<Config> {
    let base_config = Figment::new()
        .merge(Serialized::defaults(Config::default()))
        .merge(("verbosity", TraceLevel::from(&cli.verbosity)))
        .merge(("log_file", &cli.log_file));

    let figment = match (
        &cli.config,
        &cli.game_config,
        &cli.agent_config,
        &cli.seed_config,
    ) {
        (Some(config_path), None, None, None) => base_config.merge(Yaml::file(config_path)),
        (None, Some(game_config_path), None, _) => {
            let game_config: game::Config = des_yaml_file(game_config_path, "game config")?;
            base_config.merge(("game_config", game_config))
        }
        (None, None, Some(agent_config_path), None) => {
            let agent_config: agent::Config = des_yaml_file(agent_config_path, "agent config")?;
            base_config.merge(("agent_config", agent_config))
        }
        (None, None, Some(agent_config_path), Some(seed)) => {
            let game_config = game::Config::new_random_with_seed(*seed);
            let agent_config: agent::Config = des_yaml_file(agent_config_path, "agent config")?;
            base_config
                .merge(("game_config", game_config))
                .merge(("agent_config", agent_config))
        }
        (None, Some(game_config_path), Some(agent_config_path), _) => {
            let game_config: game::Config = des_yaml_file(game_config_path, "game config")?;
            let agent_config: agent::Config = des_yaml_file(agent_config_path, "agent config")?;
            base_config
                .merge(("game_config", game_config))
                .merge(("agent_config", agent_config))
        }
        (None, None, None, Some(seed)) => {
            let game_config = game::Config::new_random_with_seed(*seed);
            base_config.merge(("game_config", game_config))
        }
        (None, None, None, None) => base_config,
        _ => unreachable!(),
    };

    let figment = match &cli.command {
        cli::Command::BatchRun(batch_config) => {
            figment.merge(("batch_config", BatchConfig::from(batch_config)))
        }
        _ => figment,
    };

    Ok(figment.extract()?)
}
