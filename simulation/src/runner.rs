use rayon::prelude::*;

use agents::agent::{self, Agent, Obs};
use puzzle::game;
use tracing::{debug, info};

use crate::config;

pub fn play_agent(game_config: game::Config, agent_config: agent::Config) -> game::Results {
    let mut agent = agent_config.build(&game_config);
    let mut game_state = game::State::from_config(game_config);
    let game_results: game::Results = rollout(&mut game_state, agent.as_mut());
    game_results
}

pub fn rollout<T: Agent + ?Sized>(game_state: &mut game::State, agent: &mut T) -> game::Results {
    let mut event = game::Event::Init;
    while !game_state.is_done() {
        match event {
            game::Event::Init | game::Event::Eat => debug!("\n{game_state}"),
            game::Event::Move | game::Event::Done | game::Event::AlreadyEnded => {}
        }
        let dir = agent.act(Obs(game_state, event));
        event = game_state.step(dir);
    }

    debug!("\n{game_state}");
    let results = game_state.results();
    info!("Game ended with {results}");
    results
}

pub fn batch_play_agent(
    game_config: game::Config,
    agent_config: agent::Config,
    batch_config: config::BatchConfig,
) -> Vec<game::Results> {
    rayon::ThreadPoolBuilder::new()
        .num_threads(batch_config.threads)
        .build_global()
        .unwrap();

    (0..batch_config.n_runs)
        .into_par_iter()
        .map(|i| {
            let mut game_config = game_config.clone();
            game_config.add_to_seed(i as u64);
            let agent_config = agent_config.clone();
            play_agent(game_config, agent_config)
        })
        .collect()
}
